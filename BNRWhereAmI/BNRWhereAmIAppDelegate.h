//
//  BNRWhereAmIAppDelegate.h
//  BNRWhereAmI
//
//  Created by shashi kumar on 2/4/14.
//  Copyright (c) 2014 HCS. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BNRWhereAmIAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
