//
//  main.m
//  BNRWhereAmI
//
//  Created by shashi kumar on 2/4/14.
//  Copyright (c) 2014 HCS. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "BNRWhereAmIAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([BNRWhereAmIAppDelegate class]));
    }
}
